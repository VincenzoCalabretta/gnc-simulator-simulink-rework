%get space and timepoints and decimate
decimation_num = 1;
counter = 1;

clear waypoints;
clear elapsed_time;
for i=1:size(out.tout,1)
    if (mod(i,decimation_num)==0)

        waypoints(counter,:) = out.log_position.signals.values(i,:);
        elapsed_time(counter,:) = out.log_position.time(i,:);
        counter = counter+1;
    end
end

%generate figure and plot
figure(1);
A = [waypoints,elapsed_time];
scatter3(A(:,1),A(:,2),A(:,3),10,A(:,4),"filled");

%sarebbe comodo allineare subito la visualizzazione al piano X-Z