%------------------------------------------------------------------------
% Chaser
%--------------------------------------------------------------------------
clear;

lx   = 1.2;                     
ly   = 1.2;                     
lz   = 1.2;  
m_c0 = 200; % initial mass [kg]                  
Jx0=(m_c0*(2*lx^2)/12);          
Jy0=(m_c0*(2*ly^2)/12);       
Jz0=(m_c0*(2*lz^2)/12);

J0 =[Jx0  0   0;                 % [kg*m^2]  Inetial tensor
     0  Jy0  0;               
     0   0  Jz0];

mass_c_0 = 14;
%% load workspace to data dictionary
dd = Simulink.data.dictionary.open('../data/main_data.sldd');
dDataSectObj = getSection(dd,'Design Data');
importedVars = importFromBaseWorkspace(dd);
clear;