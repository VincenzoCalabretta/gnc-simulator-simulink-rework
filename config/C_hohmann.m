%load data dictionary
clear;

dd = Simulink.data.dictionary.open('../data/main_data.sldd');

% gather data from data dictionary
dDataSectObj = getSection(dd,"Design Data");

omega_entry = getEntry(dDataSectObj,"omega");   % - 
omega = getValue(omega_entry);                  % - steps to get omega variable from database
mass_c_0_entry = getEntry(dDataSectObj,"mass_c_0");
mass_c_0 = getValue(mass_c_0_entry);
% TODO why do you need to get the value from the database as before?
% they are not used.

% simple test hohmann calculation
delta_z = 3*10^3;
delta_V = omega/4*delta_z;
delta_t = 1;

F = mass_c_0*delta_V/delta_t;

% update entries in the data dictionary
deleteEntry(dDataSectObj,'hoh_initial_burn_time');
addEntry(dDataSectObj,'hoh_initial_burn_time',100); %_______________ [s]

deleteEntry(dDataSectObj,'hoh_initial_burn_duration');
addEntry(dDataSectObj,'hoh_initial_burn_duration',delta_t); %_______ [s]

deleteEntry(dDataSectObj,'hoh_final_burn_time');
addEntry(dDataSectObj,'hoh_final_burn_time',2879); %__________________[s]
% tempo iniziale di ottimizzazione: 2879
%


deleteEntry(dDataSectObj,'hoh_final_burn_duration');
addEntry(dDataSectObj,'hoh_final_burn_duration',delta_t); %_________ [s]

deleteEntry(dDataSectObj,'F');
addEntry(dDataSectObj,'F',F); %_________ [s]

% basically another way to add entries to the database

clear;