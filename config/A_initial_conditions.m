%% orbital elements
clear;

% FAI UNA DIFFERENZA NETTA TRA LE GRANDEZZE DEL CHASER E DEL TARGET
% PER ESEMPIO CONSIDERA DI AVERE LE GRANDEZZE IN FILE RELATIVI A GLI
% OGGETTi
% - TARGET
% - CHASER
% - FREE DRIFT
% - HOHMANN
% .....
mu     = 3.986012*10^14;                                                   % [m^3/s^2]  Standard gravitational parameter
g0     = 9.81;                                                             % [m/s^2]    Zero gravity
r_E    = 6378.145*10^3;                                                    % [m]        Earth Radius
height = 400*10^3;                                                         % [m]        Reference altitude
r      = r_E + height;                                                     % [m]        Orbita Radius
omega = (mu/r^3)^0.5                                                      % [rad]      Target angular velocity
v      = sqrt(mu/r);                                                       % [m/s]      Tangential orbital speed
T = 2*pi/omega

%% initial conditions

% TODO change in vector form
% Positions assigned in LVLH frame

%---- position ------------------------------------------------------------
%----   translational ------------------------------------------------------
x0 = -10*10^3;                                                             % [m]      X position 
y0 = 0;                                                                    % [m]      Y position

zc = 397*10^3;                                                             % [m] Chaser orbit
zt      = 400*10^3;                                                        % [m]      Target orbit
delta_z = zt - zc;                                                         % [m/s]    Difference of orbit between Chaser and Target 
z0      = delta_z;

pos0_lvlh = [x0,y0,z0];
%----   angular -----------------------------------------------------------
eul_ang0 = [0,0,0];                                                 	   % [rad]    Euler angles between the orbital 

%---- velocity ------------------------------------------------------------
%----   translational ------------------------------------------------------
Vx0  = v;                                                                  % [m/s] Initial speed with no errors in ECI Frame with null Target speed 
Vx0 = 3/2*omega*delta_z;                                                   % [m/s] Relative speed in LVLH when the Target and Chaser orbit is different

x0_dot   = Vx0;                                                              % [m/s]    X speed 
y0_dot   = 0;                                                               % [m/s]    Y speed 
z0_dot   = 0;                                                               % [m/s]    Z speed

vel0_lvlh = [x0_dot, y0_dot, z0_dot];


%CONTROLLA QUESTA PARTE NON MI PIACE
rc      = r_E + zc;                                                     % [m]      Orbit Radius
omegac_0  = (mu/rc^3)^0.5;                                                   % [rad]    Chaser angular velocity
omegac0_lvlh = [0,-omegac_0,0];                                                 % [rad/s]  Chaser angular velocity in the LVLH WHY?
%----   angular -----------------------------------------------------------

%% load workspace to data dictionary
dd = Simulink.data.dictionary.open('../data/main_data.sldd');
dDataSectObj = getSection(dd,'Design Data');
importedVars = importFromBaseWorkspace(dd); % - imports variables from matlab workspace to data dictionary of the dictionary objects
clear;
